import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './components/routes/agregar/agregar.component';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { HomeComponent } from './components/routes/home/home.component';
import { IngresoComponent } from './components/routes/ingreso/ingreso.component';
import { ListItemsComponent } from './components/routes/list-items/list-items.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { SeriesComponent } from './components/routes/series/series.component';

const routes: Routes = [
{
  path: 'ingreso', component: IngresoComponent
},
{
  path: 'home', component: HomeComponent
},
{
  path: 'list', component: ListItemsComponent
},
{
  path: 'peliculas', component: PeliculasComponent
},
{
  path: 'series', component: SeriesComponent
},
{
  path: 'dash', component: DashboardComponent
},
{
  path: 'agre', component: AgregarComponent
},
{
  path: '**', redirectTo: 'home'
},
{
  path: '', component: HomeComponent
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
