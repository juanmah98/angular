import { Injectable } from '@angular/core';
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyACAsu5QaclAmQuo5_m8b-Rpu9P001hC5s",
  authDomain: "pelisup-sj.firebaseapp.com",
  projectId: "pelisup-sj",
  storageBucket: "pelisup-sj.appspot.com",
  messagingSenderId: "511537116427",
  appId: "1:511537116427:web:640006d54f2401f59f5e0e",
  measurementId: "G-YN398C21V7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

@Injectable({
  providedIn: 'root'
})
export class FireBaseService {

  constructor() { }
}
