import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pelisup } from 'src/interfaces/Pelisup';
import { MoviesAPI } from 'src/interfaces/MoviesApi';
import { SeriesAPI } from 'src/interfaces/SeriesApi';
import { TrendingAPI } from 'src/interfaces/TrendingApi';
// import { AngularFireStorageModule } from 'angularfire2/storage';
// import { FirebaseStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MovieSerieBase } from 'src/interfaces/MovieSerieBase';
import { AuthServiceService } from './auth-service.service';
import { user } from 'rxfire/auth';
import { Userrr } from 'src/interfaces/Userrr';


@Injectable({
  providedIn: 'root'
})

export class MovieServicesService {

  appId: string = '6eb69fe7e678bb0ad6fdecc89cfd6f5d';
  baseURL: string = 'https://api.themoviedb.org/3';
  clave: any = '';

  constructor(private http: HttpClient,
    public firestore: AngularFirestore) {  }

 
    
  addUser(userId: MovieServicesService): Promise <any>{
    return this.firestore.collection('Usuario').add(userId);
  }

  
  addItem(userId: AuthServiceService, item: MovieSerieBase){  
   
    console.log("userID:",userId);
    return this.firestore.collection('Usuarios').doc(`${userId}`).collection('movie').add(item)
  }

  getList(userId: MovieServicesService){
    console.log('userId:', userId)    
    return this.firestore.collection('Usuarios').doc(`${userId}`).collection('movie').snapshotChanges();
  }

  deleteItem(idUser: string, id: string): Promise<any>{
    console.log(idUser, id)
    return this.firestore.collection(`Usuarios/${idUser}/movie`).doc(id).delete();
  }

  getMovies(): Observable<MoviesAPI>{

    let params = new HttpParams().set('api_key', this.appId);
    return this.http.get<MoviesAPI>(this.baseURL + '/movie/popular' ,{
      params: params
    })    
  }

  getSeries(): Observable<SeriesAPI>{

    let params = new HttpParams().set('api_key', this.appId);
    return this.http.get<SeriesAPI>(this.baseURL + '/tv/popular' ,{
      params: params
    })    
  }

  getTrending(): Observable<TrendingAPI>{

    let params = new HttpParams().set('api_key', this.appId);
    return this.http.get<TrendingAPI>(this.baseURL + '/trending/all/week' ,{
      params: params
    })    
  }
  

}
