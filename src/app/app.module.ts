import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/layout/header/header.component';
import { HomeComponent } from './components/routes/home/home.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { IngresoComponent } from './components/routes/ingreso/ingreso.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { LayoutModule } from './components/layout/layout.module';
import { RoutesModule } from './components/routes/routes.module';
import { SharedModule } from './components/shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireModule } from '@angular/fire/compat';
// import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { StorageModule } from '@angular/fire/storage';




@NgModule({
  declarations: [
    AppComponent,
    
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,    
    RouterModule,
    SharedModule,
    LayoutModule,
    HttpClientModule,
    RoutesModule,
    FormsModule,   
    ReactiveFormsModule,
    AngularFireAuthModule,    
    AngularFireModule.initializeApp((environment.firebase)), provideAuth(() => getAuth()),
    // AngularFireStorageModule,
    AngularFirestoreModule,
    StorageModule
    
    // , provideFirebaseApp(() => initializeApp(environment.firebase)), provideAuth(() => getAuth())
    

  ],
  providers: [],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
