import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { MovieSerieBase } from 'src/interfaces/MovieSerieBase';
import { Pelisup } from 'src/interfaces/Pelisup';
import { ResultMovie } from 'src/interfaces/ResultMovieAPI';
import { ResultSerie } from 'src/interfaces/ResultSerieAPI';
import { Result, TrendingAPI } from 'src/interfaces/TrendingApi';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  public userLogged: Observable<any> = this.authServiceService.afauth.user;

  constructor(private authServiceService: AuthServiceService,private _MovieServicesService: MovieServicesService) { }
  
  ngOnInit(): void {
  }
  
  @Input() serieRe: ResultSerie[] = []
  @Input() movieRe: ResultMovie[] = []
  @Input() movies_series: Result[] = []
  @Input() card: MovieSerieBase[] = []
  @Input() movies: MovieSerieBase[] = []
  

  public addList(item: MovieSerieBase) {
    let user = JSON.parse(localStorage.getItem('usuario') || '');
    
    this._MovieServicesService
      .addItem(user.uid, item)
      .then(() => {
        console.log('se agregó el item al usuario');
      })
      .catch((error) => {
        console.log(error);
      });
  }

  deleteItem(id: string){
    let user = JSON.parse(localStorage.getItem('usuario') || '');
    this._MovieServicesService.deleteItem(user.uid, id).then(()=>{
      console.log("se elimino correctamente")
    }).catch(error=>{
      console.log(error)
    })
  }



 


   


}
