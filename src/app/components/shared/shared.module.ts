import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../layout/header/header.component';
import { CardComponent } from './card/card.component';
import { RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [CardComponent],
  imports: [
    CommonModule, RouterModule
  ],

  exports: [CardComponent]
})
export class SharedModule { }
