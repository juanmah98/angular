import { Component, NgModule, OnInit, Input } from '@angular/core';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { MoviesAPI } from 'src/interfaces/MoviesApi';
import { MovieSerieBase } from 'src/interfaces/MovieSerieBase';
import { Pelisup } from 'src/interfaces/Pelisup';
import { ResultMovie } from 'src/interfaces/ResultMovieAPI';
import { ResultSerie } from 'src/interfaces/ResultSerieAPI';
import { SeriesAPI } from 'src/interfaces/SeriesApi';
import { Result, TrendingAPI } from 'src/interfaces/TrendingApi';
// import { Result } from 'src/interfaces/ResultAPI';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MovieServicesService],
})
export class HomeComponent implements OnInit {

  constructor(private _MovieServicesService: MovieServicesService) { }

  trendingColeccion: Result[] = [];

  tipo = 1;
  toSearch="";
  
  ngOnInit(): void {        
    this.getTrending();
  }

  getTrending(){
    this._MovieServicesService.getTrending().subscribe({
      next: (data: TrendingAPI) => {
        console.log(data.results);
        this.trendingColeccion=data.results;
        
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
      }
      
    })
  }

  state1 = false;
  state2 = false;
  state3 = false;
  
  movies_series: Result[] = this.trendingColeccion;
  
 
 
 
  

//   movies_series: Pelisup[] = [
  
//    {
//     id: 1,
//     name: "Scream",
//     description: "Veinticinco años después de que una racha de brutales asesinatos conmocionara a la tranquila ciudad de Woodsboro, un nuevo asesino se ha puesto la máscara de Ghostface y comienza a apuntar a un grupo de adolescentes para resucitar los secretos del pasado mortal de la ciudad.",
//     img: "../../../../assets/recursos/portadas/nuevas/Scream.jpg",
//     rating: "7",
//     category: "pelicula",
    
//   },
//   {
//     id: 2,
//     name: "The Walking Dead",
//     description: "El ayudante del alguacil, Rick Grimes, despierta de un coma y se encuentra con un mundo posapocalíptico dominado por zombis carnívoros. Se propone encontrar a su familia y se encuentra con muchos otros sobrevivientes en el camino.",
//     img: "../../../../assets/recursos/portadas/nuevas/The Walking Dead.jpg",
//     rating: "8.1",
//     category: "serie",     
    
//   }, 
//   {
//     id: 3,
//     name: "Transportador 3",
//     description: "Frank Martin se pone los guantes de conducir para llevar a Valentina, la hija secuestrada de un funcionario del gobierno ucraniano, de Marsella a Odessa en el Mar Negro. En el camino, tiene que lidiar con matones que quieren interceptar la entrega segura de Valentina y no dejar que sus sentimientos personales se interpongan en el camino de su peligroso objetivo.",
//     img: "../../../../assets/recursos/portadas/nuevas/Transportador 3.jpg",
//     rating: "6.1",
//     category: "pelicula",  

//   }, 
//   {
//     id: 4,
//     name: "Spider-Man: Sin camino a casa",
//     description: "Peter Parker está desenmascarado y ya no puede separar su vida normal de las altas apuestas de ser un superhéroe. Cuando le pide ayuda al Doctor Strange, lo que está en juego se vuelve aún más peligroso y lo obliga a descubrir lo que realmente significa ser Spider-Man.",
//     img: "../../../../assets/recursos/portadas/nuevas/Spider-man.jpg",
//     rating: "8.4",
//     category: "pelicula", 

//   }, 
//   {
//     id: 5,
//     name: "Peaky Blinders",
//     description: "Una epopeya familiar de gánsteres ambientada en 1919 en Birmingham, Inglaterra, y centrada en una pandilla que cose cuchillas de afeitar en los picos de sus gorras, y su feroz jefe Tommy Shelby, que pretende ascender en el mundo.",
//     img: "../../../../assets/recursos/portadas/nuevas/peaky blinders.jpg",
//     rating: "8.6",   
//     category: "serie", 
   
//   }, 
//   {
//     id: 6,
//     name: "Lucifer",
//     description: "Aburrido e infeliz como el Señor del Infierno, Lucifer Morningstar abandonó su trono y se retiró a Los Ángeles, donde se asoció con la detective del Departamento de Policía de Los Ángeles, Chloe Decker, para acabar con los criminales. Pero cuanto más tiempo esté lejos del inframundo, mayor será la amenaza de que lo peor de la humanidad pueda escapar.",
//     img: "../../../../assets/recursos/portadas/nuevas/Lucifer.jpg",
//     rating: "8.8",  
//     category: "serie", 
   
//   }, 
//   {
//     id: 7,
//     name: "Cobra Kai",
//     description: "Esta serie de secuelas de Karate Kid comienza 30 años después de los eventos del Torneo de Karate All Valley de 1984 y encuentra a Johnny Lawrence en la búsqueda de la redención al reabrir el infame dojo de karate Cobra Kai. Esto reaviva su antigua rivalidad con el exitoso Daniel LaRusso, quien ha estado trabajando para mantener el equilibrio en su vida sin su mentor, el Sr. Miyagi.",
//     img: "../../../../assets/recursos/portadas/nuevas/Cobra Kai.jpg",
//     rating: "8.1",
//     category: "serie", 
    
//   }, 
//   {
//     id: 8,
//     name: "The King's Man",
//     description: "Mientras una colección de los peores tiranos y mentes criminales de la historia se reúnen para planear una guerra para acabar con millones, un hombre debe correr contra el tiempo para detenerlos.",
//     img: "../../../../assets/recursos/portadas/nuevas/The King's Man.jpg",
//     rating: "7.1",
//     category: "pelicula", 
   
//   },
//   {
//     id: 9,
//     name: "The Matrix Resurrections",
//     description: "Plagada de extraños recuerdos, la vida de Neo da un giro inesperado cuando se encuentra de nuevo dentro de Matrix",
//     img: "../../../../assets/recursos/portadas/nuevas/The Matrix Resurrections.jpg",
//     rating: "6.8",
//     category: "pelicula", 
   
//   },
//   {
//     id: 10,
//     name: "Fast and Furious 9",
//     description: "Dominic Toretto y su equipo luchan contra el asesino más hábil y el conductor de alto rendimiento que jamás hayan conocido: su hermano abandonado.",
//     img: "../../../../assets/recursos/portadas/nuevas/f9.jpg",
//     rating: "7.1",
//     category: "pelicula", 
   
//   },
//   {
//     id: 11,
//     name: "Moonfall",
//     description: "Una fuerza misteriosa saca a la luna de su órbita alrededor de la Tierra y la lanza en un curso de colisión con la vida tal como la conocemos.",
//     img: "../../../../assets/recursos/portadas/nuevas/Moonfall.jpg",
//     rating: "5.9",
//     category: "pelicula", 
   
//   },
//   {
//     id: 12,
//     name: "Euphoria ",
//     description: "Un grupo de estudiantes de secundaria navega por el amor y las amistades en un mundo de drogas, sexo, trauma y redes sociales.",
//     img: "../../../../assets/recursos/portadas/nuevas/Euphoria.jpg",
//     rating: "8.4",
//     category: "serie", 
   
//   },

// ]

series: Result[] = [];
movies: Result[] = [];
todos: Result[] = [] = this.trendingColeccion;
cont = 0;

seccion(id: number){
  this.state1=false;
  this.state2= false;
  this.state3= false;
  console.log('FUNCIONA')
  if(id!=1)
  {console.log('Dentro if')
    if(id==2)
      {console.log('Dentro if id 2')
        this.state2=true; 
        
      //console.log('entra al if 2')
      console.log('Entrando FOR')
        for(let i = 0; i < this.trendingColeccion.length; i++){
        // console.log('entra al for')
        console.log('Dentro FOR')
          if( this.trendingColeccion[i].media_type=="movie"){
            console.log('Dentro if de FOR')
            this.movies[this.cont]= this.trendingColeccion[i];
            this.card[this.cont]= this.movies[this.cont];
            this.tipo=id;
            this.cont++;
            console.log(this.movies[this.cont]);
            //console.log('fin for')
          }
          
        } this.todos=this.movies;
        console.log('Fuera FOR')
    }else if(id==3)
      {
        this.state3=true;
        
        for(let i = 0; i < this.trendingColeccion.length; i++){
        // console.log('entra al for')
        if(this.trendingColeccion[i].media_type=="tv"){
    
          this.series[this.cont]=this.trendingColeccion[i];
          this.tipo=id;
          this.cont++;
          //console.log('fin for')
        }
       
      }this.todos=this.series;
    }
    //console.log('entra al if1')
  }else {console.log('Dentro if id 1')
    this.tipo=1
    this.todos=this.trendingColeccion;
    this.state1=true;
    
  
  }

  this.cont=0;
  
  
  
}

setSeccion(){
  return this.tipo;
}

 search(value:string){
    this.tipo=4   
    this.state1=true; 
    this.state2=false; 
    this.state3=false; 
    
    if(value!=" "){
    // this.todos = this.movies_series.filter(elemet => elemet.name.toLowerCase()[this.todos.length] ==value.toLowerCase()[this.todos.length])
    this.todos = this.trendingColeccion.filter(elemet => elemet.title?.toLowerCase().includes(value)||elemet.name?.includes(value))
       
    console.log(this.todos);
       
    }
    else{
      this.todos=this.movies_series;
    }
   
     

}

seccion2(value: number){
  return 2;
}

card: MovieSerieBase[] =[];
public contt: number = 0;

// public addList(item: MovieSerieBase) {
 
  
//   console.log(item);
//   let user = JSON.parse(localStorage.getItem('Usuario') || '');
//   this._MovieServicesService
//     .addItem(user.uid, item)
//     .then(() => {
//       console.log('se agregó el item al usuario');
//     })
//     .catch((error) => {
//       console.log(error);
//     });
//     console.log('fin add');
// }







     

 

}
