// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
// import { AuthServiceService } from 'src/app/services/auth-service.service';

// @Component({
//   selector: 'app-ingreso',
//   templateUrl: './ingreso.component.html',
//   styleUrls: ['./ingreso.component.css']
// })
// export class IngresoComponent implements OnInit {

//   miFormulario: FormGroup = this.fb.group({
//     email: [, [Validators.required, Validators.minLength(3)]],
//     password: [, [Validators.required, Validators.minLength(4)]],
    
    
//   })


 
//   constructor(private fb: FormBuilder, private _authService: AuthServiceService) { }

//   ngOnInit(): void {
//   }

//   campoEsValido(campo: string){
//     return(
//     this.miFormulario.controls[campo].errors &&
//     this.miFormulario.controls[campo].touched
//     );
//   }
 

//   guardar() {
//     console.log('Guardando');
//     if (this.miFormulario.invalid) {
//       this.miFormulario.markAllAsTouched();
//       return;
//     }
    
//     console.log(this.fb);
//     console.log(this.email, this.password)
//     // console.log(this.miFormulario.value);
//     this._authService.login(this.email, this.password);
   
//     this.miFormulario.reset();
//     this.setLocalStorage();
//   }

//   usuario: any = {
//     email:  this.email,
//     password: this.password

//   } 

//   logInWithGoogleUser(){
//     this._authService.loginWithGoogle(this.email, this.password);
//     console.log(this.email, this.password)
//     this.setLocalStorage();
//   }

//   setLocalStorage() {
//     console.log('2');
//     console.log('Usuario',this.usuario);
//     localStorage.setItem('Usuario', JSON.stringify(this.usuario));
//   }
  

// }

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'firebase/auth';
import { user } from 'rxfire/auth';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { Userrr } from 'src/interfaces/Userrr';

@Component({
    selector: 'app-ingreso',
    templateUrl: './ingreso.component.html',
    styleUrls: ['./ingreso.component.css']
  })
  export class IngresoComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),],],
    password: [, [Validators.required, Validators.minLength(6)]],
    
  });

  constructor(private fb: FormBuilder, private authService: AuthServiceService, private router: Router, public _clave: MovieServicesService) {}

  ngOnInit(): void {}

  @Input() bandera: boolean = false; 

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

  Ingresar(){
    if(this.miFormulario.invalid)
      return
    const { email, password } = this.miFormulario.controls;
    this.authService.login(email.value, password.value).then(response => {
      if(!response)
        return
      localStorage.setItem('usuario', JSON.stringify(response.user))
     
      // this._clave.addClave(response.user?.email);
      // console.log("_clave:",this._clave.clave)
      // console.log(this.clave.id);
      console.log("Se registro: ", response)
      this.router.navigate(['/home'])
    })
    this.bandera = true;
  }

  clave: Userrr = {
    id: "indefinido"
  };

  IngresarConGoogle(){
    this.authService.loginWithGoogle().then(response => {
      if(!response)
        return
      localStorage.setItem('usuario', JSON.stringify(response.user))
      console.log("Se registro: ", response, response.user?.email)
      // this.clave.id =  (response.user?.email) as string;
      // this._clave.addClave(response.user?.email);
      // console.log("_clave:",this._clave.clave)
      // console.log(this.clave.id);
      this.router.navigate(['/home'])
    })
    this.bandera = true;
  }

  LogOut(){
    // this._clave.clear();
    console.log("clave logout:",this._clave.clave)
    this.bandera = false;

  }
}