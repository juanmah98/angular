import { Component, Input, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { MovieSerieBase } from 'src/interfaces/MovieSerieBase';
import { ListItemsComponent } from '../list-items/list-items.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user: any;

  movies: MovieSerieBase[]=[];
  constructor(private _movieSeriesService: MovieServicesService,private _authService: AuthServiceService) { }
 

  ngOnInit(): void {
     this.user = JSON.parse(localStorage.getItem('usuario') || '');
    this.getMovies();
  } 

  getMovies()
  {
    console.log("user:" ,this.user.uid);
    this._movieSeriesService.getList(this.user.uid).subscribe(
      response => {
        
        this.movies = []
        console.log("esto es response", response)
        response.forEach((element: any) => {
          // console.log(element.payload.doc.id)
          // console.log(element.payload.doc.data())
          this.movies.push({
            idGlobal: element.payload.doc.id,
            ...element.payload.doc.data(),
          })
        })
        console.log("esta es la peticon de movies", this.movies)
        
      },
      error => {
        console.log("fallo la peticion de movies", error)
      }
    )
    console.log("fin get");
  }

}
