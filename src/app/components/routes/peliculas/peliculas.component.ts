import { Component, OnInit, Input } from '@angular/core';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { MoviesAPI, Result } from 'src/interfaces/MoviesApi';
import { ResultMovie } from 'src/interfaces/ResultMovieAPI';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {

  constructor(private _MovieServicesService: MovieServicesService) { }

  movieColeccion: ResultMovie[] = [];
  toSearch="";
  tipo = 2;
  ngOnInit(): void {
    this.getMovies();
  }

  getMovies(){
    this._MovieServicesService.getMovies().subscribe({
      next: (data: MoviesAPI) => {
        console.log(data);
        this.movieColeccion=data.results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
      }
      
    })
  }

  todos: ResultMovie[] = this.movieColeccion;

  setSeccion(){
    return this.tipo;
  }

  search(value:string){
   
    
    if(value!=" "){

    // this.todos = this.movies_series.filter(elemet => elemet.name.toLowerCase()[this.todos.length] ==value.toLowerCase()[this.todos.length])
    this.tipo=1;
    this.todos = this.movieColeccion.filter(elemet => elemet.title?.toLowerCase().includes(value.toLowerCase()))
   
    
    console.log(this.todos);
   
    }
    else{
      this.todos=this.movieColeccion;
      this.tipo=2;
    }
   
     

}
  

}
