import { Component, OnInit } from '@angular/core';
import { MovieServicesService } from 'src/app/services/movie-services.service';
import { ResultSerie } from 'src/interfaces/ResultSerieAPI';
import { SeriesAPI } from 'src/interfaces/SeriesApi';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

  constructor(private _MovieServicesService: MovieServicesService) { }

  serieColeccion: ResultSerie[] = [];
  toSearch="";
  tipo=2;

  ngOnInit(): void {
    this.getSeries();
  }

  getSeries(){
    this._MovieServicesService.getSeries().subscribe({
      next: (data: SeriesAPI) => {
        console.log(data);
        this.serieColeccion=data.results;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La peticion termino')
      }
      
    })
  }

  setSeccion(){
    return this.tipo;
  }

  todos: ResultSerie[] = this.serieColeccion;
  search(value:string){
   
    
    if(value!=" "){

    // this.todos = this.movies_series.filter(elemet => elemet.name.toLowerCase()[this.todos.length] ==value.toLowerCase()[this.todos.length])
    this.tipo=1;
    this.todos = this.serieColeccion.filter(elemet => elemet.name?.toLowerCase().includes(value.toLowerCase()))
   
    
    console.log(this.todos);
   
    }
    else{
      this.todos=this.serieColeccion;
      this.tipo=2;
    }
   
     

}

}
