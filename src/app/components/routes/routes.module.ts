import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { SeriesComponent } from './series/series.component';
import { IngresoComponent } from './ingreso/ingreso.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ListItemsComponent } from './list-items/list-items.component';



@NgModule({
  declarations: [
    PeliculasComponent,
    SeriesComponent,
    IngresoComponent,
    HomeComponent,
    DashboardComponent,
    AgregarComponent,
    ListItemsComponent
  ],
  imports: [
    CommonModule, RouterModule, SharedModule, FormsModule, ReactiveFormsModule
  ],

  exports: [HomeComponent, IngresoComponent, PeliculasComponent, SeriesComponent, DashboardComponent, AgregarComponent, ListItemsComponent]
})
export class RoutesModule { }
