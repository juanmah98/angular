import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthServiceService } from 'src/app/services/auth-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userLogged: Observable<any> = this.authServiceService.afauth.user;

  constructor(private authServiceService: AuthServiceService) { }

  ngOnInit(): void {    
  }

  public logOut(){
    this.authServiceService.logout();
  }

  
 

}
