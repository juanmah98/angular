export interface Pelisup{
    name:        string;
    id:          number;
    description: string;
    img:         string;
    rating:      string;
    category:    string;
    
}
