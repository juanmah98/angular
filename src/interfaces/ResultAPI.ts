export interface Result {
    video:             boolean;
    vote_average:      number;
    overview:          string;
    release_date:      Date;
    vote_count:        number;
    adult:             boolean;
    backdrop_path:     string;
    id:                number;
    genre_ids:         number[];
    title:             string;
    original_language: string;
    original_title:    string;
    poster_path:       string;
    popularity:        number;
    media_type:        string;
    name:              string;
   
}
