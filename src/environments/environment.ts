// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'pelisup-sj',
    appId: '1:511537116427:web:640006d54f2401f59f5e0e',
    storageBucket: 'pelisup-sj.appspot.com',
    apiKey: 'AIzaSyACAsu5QaclAmQuo5_m8b-Rpu9P001hC5s',
    authDomain: 'pelisup-sj.firebaseapp.com',
    messagingSenderId: '511537116427',
    measurementId: 'G-YN398C21V7',
  },
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyACAsu5QaclAmQuo5_m8b-Rpu9P001hC5s",
    authDomain: "pelisup-sj.firebaseapp.com",
    projectId: "pelisup-sj",
    storageBucket: "pelisup-sj.appspot.com",
    messagingSenderId: "511537116427",
    appId: "1:511537116427:web:640006d54f2401f59f5e0e",
    measurementId: "G-YN398C21V7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
